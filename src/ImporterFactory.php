<?php

namespace Afom\TranslationManagerLaravel;

use Afom\TranslationManager\Importer\ExternalCsvImporter;
use Afom\TranslationManager\Importer\Parser\CsvParser;
use Afom\TranslationManager\Exception\ConfigException;
use Symfony\Component\Filesystem\Filesystem;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use GuzzleHttp\Client;

class ImporterFactory
{
    public function create($type)
    {
        switch ($type) {
            case 'external':
                return new ExternalCsvImporter(new Client(), new CsvParser(ReaderFactory::create(Type::CSV), new Filesystem()));
            default:
                throw new ConfigException('Invalid importer type given');
        }
    }
}

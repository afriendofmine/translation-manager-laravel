<?php

namespace Afom\TranslationManagerLaravel\Commands;

use Afom\TranslationManager\Importer\ImporterInterface;
use Afom\TranslationManager\Writer\JSONWriter;
use Afom\TranslationManager\Writer\PHPWriter;
use Afom\TranslationManager\Writer\WriterInterface;
use Afom\TranslationManagerLaravel\ConfigManager;
use Illuminate\Console\Command;

class UpdateTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update translations from Google.';

    /** @var ConfigManager */
    private $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        parent::__construct();

        $this->configManager = $configManager;
    }

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle()
    {
        foreach ($this->configManager->getWritersTypes() as $type) {

            try {
                $writer = $this->getWriter($type);

                if (true === $this->configManager->merge($type)) {
                    $translations = [];

                    foreach ($this->configManager->getSources() as $sourceOption) {
                        $translations = array_merge($translations, app(ImporterInterface::class)->import($sourceOption['source']));
                    }

                    $writer->write($translations, 'app');
                }

                if (false === $this->configManager->merge($type)) {
                    foreach ($this->configManager->getSources() as $sourceOption) {
                        $translations = app(ImporterInterface::class)->import($sourceOption['source']);
                        $writer->write($translations, $sourceOption['filename']);
                    }
                }
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
    }

    /**
     * @param string $type
     *
     * @return WriterInterface
     *
     * @throws \Exception
     */
    private function getWriter($type)
    {
        if ($type === 'json') {
            return app(JSONWriter::class);
        }

        if ($type === 'php') {
            return app(PHPWriter::class);
        }

        throw new \Exception(sprintf('Writer %s not defined', $type));
    }
}

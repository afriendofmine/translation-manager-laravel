<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Importers configuration
    |--------------------------------------------------------------------------
    |
    */
    'importers' => [

        'adapter' => 'external',

        'external' => [
            // sheets
            [
                'filename' => '',
                'source'   => '',
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Writers configuration
    |--------------------------------------------------------------------------
    |
    */
    'writers'   => [
        'json' => [
            'structure'   => 'flattened',
            'destination' => base_path('resources/assets/lang'),
            'merge'       => true,
        ],
        'php'  => [
            'structure'   => 'nested',
            'destination' => base_path('resources/lang'),
            'merge'       => false,
        ],
    ],
];

<?php

namespace Afom\TranslationManagerLaravel;

use Afom\TranslationManager\Importer\ImporterInterface;
use Afom\TranslationManagerLaravel\Commands\UpdateTranslations;
use Afom\TranslationManager\Importer\ExternalCsvImporter;
use Afom\TranslationManager\Importer\Parser\CsvParser;
use Afom\TranslationManager\Writer\Mapper\ArrayMapper;
use Afom\TranslationManager\Writer\JSONWriter;
use Afom\TranslationManager\Writer\PHPWriter;
use Symfony\Component\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use GuzzleHttp\Client;

class TranslationManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // register command
        if ($this->app->runningInConsole()) {
            $this->commands([UpdateTranslations::class]);
        }

        // copy Config to Laravel config folder in case of "php artisan vendor:publish"
        $this->publishes([
            __DIR__ . '/Config/Config.php' => config_path('translation-manager.php'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Registering only in CLI mode
        if ($this->app->runningInConsole()) {

            // register config manager
            $this->app->singleton(ConfigManager::class, function() {
                return new ConfigManager(config('translation-manager', []));
            });

            // register importer
            $this->app->singleton(ImporterInterface::class, function() {
                $type = config('translation-manager.importers.adapter', 'external');

                return (new ImporterFactory())->create($type);
            });

            // register PHP writer
            $this->app->singleton(PHPWriter::class, function () {
                $destination = config('translation-manager.writers.php.destination', '');
                $structure = config('translation-manager.writers.php.structure', '');

                return new PHPWriter(new Filesystem(), new ArrayMapper($structure), $destination);
            });

            // register JSON writer
            $this->app->singleton(JSONWriter::class, function () {
                $destination = config('translation-manager.writers.json.destination', '');
                $structure = config('translation-manager.writers.json.structure', '');

                return new JSONWriter(new Filesystem(), new ArrayMapper($structure), $destination);
            });
        }
    }
}

<?php

namespace Afom\TranslationManagerLaravel;

use Afom\TranslationManager\Exception\ConfigException;

class ConfigManager
{
    /** @var array */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getWritersTypes()
    {
        return array_keys($this->config['writers']);
    }

    /**
     * @param string $type
     *
     * @return boolean
     */
    public function merge($type)
    {
        if (false === isset($this->config['writers'][$type]['merge'])) {
            return false;
        }

        return (bool) $this->config['writers'][$type]['merge'];
    }

    /**
     * @return array
     *
     * @throws ConfigException
     */
    public function getSources()
    {
        if (!isset($this->config['importers']['adapter']) || $this->config['importers']['adapter'] === null) {
            throw new ConfigException('No import adapter defined.');
        }

        switch ($this->config['importers']['adapter']) {
            case 'external':
                return $this->config['importers']['external'];
            default:
                throw new ConfigException('No sources found.');
        }
    }
}

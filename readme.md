Translation Manager Laravel
===========================

This package contains a wrapper for the translation manager to make it easy import translation labels from google doc.

## Requirements ##

Laravel 5.2 or later


Installation
------------
Installation is a quick 3 step process:

1. Download translation-manager-laravel using composer
2. Enable the package in app.php
3. Configure your config file


### Step 1: Download afom/translation-manager-laravelusing composer

Add afom/translation-manager-laravel by running the command:

```bash
composer require afom/translation-manager-laravel
```

### Step 2: Enable the package in app.php

Register the Service in: **config/app.php**

```php
Afom\TranslationManagerLaravel\TranslationManagerServiceProvider::class,
```

### Step 3: Configure config file

Create the config file with:

```php
php artisan vendor:publish
```

Edit the config file in:

**config/translation-manager.php**

Usage:
------

Or use artisan command
```php
php artisan update:translations
```

## Questions?
You can reach us at **development@afriendofmine.nl**
